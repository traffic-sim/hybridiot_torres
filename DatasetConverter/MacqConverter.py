import pandas as pd
import sumolib
from pandas.core.frame import DataFrame
from tqdm import tqdm

from DatasetConverter.DatasetConverter import DatasetConverter
import datetime
import numpy as np


class MacqConverter(DatasetConverter):
    # key -> fname, value -> the edge mapping for the (key) dataset. An edge mapping is a dict that maps OSM_ID with
    # their correspondent edge in the SUMO network
    edge_mappings_dict = {}
    # key -> fname, value -> the time stamps for the (key) dataset
    ts_dict = {}
    # key -> fname, value -> the time stamps, in seconds, for the (key) dataset
    ts_sumo_dict = {}

    @classmethod
    def osm_id_to_edge_id(cls, osm_id_series, the_net):
        """
        Map OSM ID to SUMO edge ID

        :param osm_id_series: a LIST of OSM ID (strings)
        :param the_net: the road NETWORK (sumolib.net object)
        :return: a DICT, key is the osm id, value the SUMO edge id
        """
        osm_id_mapping = {}
        not_mapped = []
        for osm_id in tqdm(osm_id_series):
            sp = osm_id.split('_')
            u = sp[0]
            v = sp[1]

            if the_net.hasNode(v) and the_net.hasNode(u):
                from_u_to_v = list(filter(lambda e: e.getFromNode().getID() == u and
                                                    e.getToNode().getID() == v, the_net.getEdges()))
                if len(from_u_to_v) > 0 and osm_id not in osm_id_mapping:
                    osm_id_mapping[osm_id] = from_u_to_v[0].getID()
                    continue
            not_mapped.append(osm_id)

        return osm_id_mapping

    @classmethod
    def to_dataframe(cls, dataset_fname: str, var: dict = None) -> DataFrame:
        """
        input dataset from macq -> dataframe

        the output dataframe contains
        - time stamps as index
        - each row is the edge ID in sumo input network
        - each value is the traffic count

        :param dataset_fname: the PARQUET file containing the data from Macq
        :param var: contains the value of 'net' contains the path to the FILENAME of the SUMO network
        :return:
        """
        net_fname = var['net']
        data_df = pd.read_parquet(dataset_fname)

        list_osm_edges = data_df.osm_id.unique()
        data_dict = {}
        for osm_id in list_osm_edges:
            data = data_df.loc[data_df['osm_id'] == osm_id].count_short
            data_dict[osm_id] = data.to_numpy()

        data_df_formatted = pd.DataFrame.from_dict(data_dict)
        data_df_formatted['ts'] = list(data_df.timestamp.unique())
        data_df_formatted.set_index('ts', inplace=True)

        edges_mapping = cls.osm_id_to_edge_id(list_osm_edges,
                                              sumolib.net.readNet(net_fname))  # dict [osm_id] -> edge_id
        osm_id_in_net = list(edges_mapping.keys())
        data_df_formatted = data_df_formatted.loc[:, osm_id_in_net]
        data_df_formatted.rename(columns=edges_mapping, inplace=True, errors='raise')

        df_time = pd.to_datetime(list(data_df.timestamp.unique())).tz_localize(None).to_series()
        df_time.sort_values(inplace=True)
        ts = df_time.to_list()
        # 00:00 corresponds to zero in simulation time. The start time in the sumo time instant should be
        # the distance between midnight and the first ts in the previous list. Contrarily, the simulation will be at 0
        # even if the input dataset doesn't start at midnight
        ts_midnight = datetime.datetime.combine(ts[0], datetime.datetime.min.time())
        start_time_sec = np.abs((ts_midnight - ts[0]).total_seconds())
        ts_sumo = [start_time_sec + (ts_other - ts[0]).total_seconds() for ts_other in ts]

        cls.edge_mappings_dict[dataset_fname] = edges_mapping
        cls.ts_dict[dataset_fname] = ts
        cls.ts_sumo_dict[dataset_fname] = ts_sumo

        return data_df_formatted
