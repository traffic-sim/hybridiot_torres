import datetime
import os
import pandas as pd
from pandas.core.frame import DataFrame

from ExperimentsConfiguration.HybridIoT_Configuration import HybridIoT_Configuration


class DatasetConverter:

    @classmethod
    def to_dataframe(cls, dataset_fname: str, var: dict = None) -> DataFrame:
        pass

    @classmethod
    def to_parquet(cls, configuration: HybridIoT_Configuration, dataset: DataFrame, output_fname: str):
        dataset.to_parquet(os.path.join(configuration.output_folder, output_fname))

    @classmethod
    def to_sumo_ts(cls, ts) -> str:
        format_string = "%Y/%m/%d %H:%M"
        return ts.strftime(format_string)

    @classmethod
    def to_sumo_csv(cls, configuration: HybridIoT_Configuration,
                    dataset: DataFrame,
                    dataset_speed: DataFrame,
                    output_fname: str):

        the_csv_pd = pd.DataFrame()
        for col in dataset.columns:
            sub_df = pd.DataFrame(dataset.loc[:, col])
            sub_df['vPKW'] = dataset_speed.loc[:, col]
            sub_df['Detector'] = [col for _ in range(len(sub_df))]
            sub_df.rename(columns={col: 'qPKW'}, inplace=True)
            sub_df['start_time'] = [cls.to_sumo_ts(ts) for ts in sub_df.index.to_list()]

            ts_list = []
            for ts in sub_df.index.to_list():
                tsts = ts + datetime.timedelta(seconds=configuration.aggregation_interval_sec)
                ts_list.append(cls.to_sumo_ts(tsts))
            sub_df['end_time'] = ts_list
            sub_df.reset_index(drop=True, inplace=True)
            sub_df.fillna(0)
            the_csv_pd = pd.concat([the_csv_pd, sub_df])
        the_csv_pd.to_csv(output_fname, sep=';', index=False)
