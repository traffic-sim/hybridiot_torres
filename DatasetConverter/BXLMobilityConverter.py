import datetime

import numpy as np
import pandas as pd
import sumolib
from pandas.core.frame import DataFrame
from sumolib.net.edge import Edge

from DatasetConverter.DatasetConverter import DatasetConverter


class BXLMobilityConverter(DatasetConverter):
    # key -> fname, value -> the edge mapping for the (key) dataset. An edge mapping is a dict that maps OSM_ID with
    # their correspondent edge in the SUMO network
    edge_mappings_dict = {}
    # key -> fname, value -> the time stamps for the (key) dataset
    ts_dict = {}
    # key -> fname, value -> the time stamps, in seconds, for the (key) dataset
    ts_sumo_dict = {}

    @classmethod
    def get_closest_edge(cls, net: sumolib.net, lon: float, lat: float, exclude=None) -> Edge | None:
        if exclude is None:
            exclude = []
        radius = 30
        x, y = net.convertLonLat2XY(lon, lat)
        edges = net.getNeighboringEdges(x, y, radius)

        if exclude:
            edges = list(filter(lambda ed: ed[0].getID() not in exclude, edges))
        if len(edges) > 0:
            edges.sort(key=lambda edge: edge[1])
            closest_edge, dist = edges[0]
            return closest_edge
        return None

    @classmethod
    def get_edge_mapping(cls, sensors_location: DataFrame, net: sumolib.net) -> tuple[dict[str, str], list[str]]:
        """
        Get the edge mapping between brussel mobility sensors and the edges in SUMO network file
        :param sensors_location: PATH to the csv file containing the coordinates of the sensors
        :param net: a sumolib.net object
        :return: a dict [sensor ID -> sumo edge ID], a list of sensors ID which have not been mapped
        """
        edge_sensors_mapping = {}
        col_to_remove = []
        mapped_edges = []
        for count, sensor in enumerate(sensors_location.id.to_list()):
            sensor_data = sensors_location.loc[sensors_location['id'] == sensor]
            lon = sensor_data.lon.to_numpy()[0]
            lat = sensor_data.lat.to_numpy()[0]
            closest_edge = cls.get_closest_edge(net, lon, lat, mapped_edges)
            if closest_edge is not None:
                edge_sensors_mapping[sensor] = closest_edge.getID()
                mapped_edges.append(closest_edge.getID())
            else:
                print(f'Unable to map sensor: {sensor}')
                col_to_remove.append(sensor)
        return edge_sensors_mapping, col_to_remove

    @classmethod
    def to_dataframe(cls, dataset_fname: str, var: dict = None) -> DataFrame:
        """
        input dataset from macq -> dataframe

        the output dataframe contains
        - time stamps as index
        - each row is the edge ID in sumo input network
        - each value is the traffic count

        :param dataset_fname: the dataset from BXL mobility
        :param var: 'net' is the PATH to the SUMO network file. 'sensors_pos_path' is the path to the csv FILE with
        sensors coordinates. If key 'speed' is present, then the dataframe return the SPEED instead of traffic counts
        :return: a DATAFRAME
        """
        net_fname = var['net']
        net = sumolib.net.readNet(net_fname)
        sensors_pos_coords = var['sensors_pos_path']

        data_df = pd.read_csv(dataset_fname, sep=";")
        list_osm_edges = data_df.Detector.unique()
        ts_list = data_df.start_time.unique()
        data_dict = {}
        for osm_id in list_osm_edges:
            field_name = 'qPKW' if 'speed' not in var else 'vPKW'
            data = data_df.loc[data_df['Detector'] == osm_id][field_name]
            data_dict[osm_id] = data.to_numpy()
        data_dict['ts'] = ts_list

        data_df_formatted = pd.DataFrame.from_dict(data_dict, orient='index').T

        # Integrate sensors from BXL mobility and Telraam
        sensors_location = pd.read_csv(sensors_pos_coords, sep=';')
        sensors_wpos = set(data_df_formatted.columns).intersection(set(sensors_location['id']))
        # Remove the columns referring to sensors that don't have a location
        data_df_formatted.drop(set(data_df_formatted.columns).difference(sensors_wpos), axis=1, inplace=True)
        sensors_location = sensors_location[sensors_location['id'].isin(sensors_wpos)]

        # Find matching SUMO edges for the BXL mobility sensors (using their lon/lat coordinates)
        edge_mapping, col_to_remove = cls.get_edge_mapping(sensors_location, net)
        data_df_formatted = data_df_formatted.loc[:, list(edge_mapping.keys())]
        data_df_formatted.rename(columns=edge_mapping, inplace=True, errors='raise')

        df_time = pd.to_datetime(data_dict['ts']).tz_localize(None).to_series()
        df_time.sort_values(inplace=True)
        ts = df_time.to_list()
        # 00:00 corresponds to zero in simulation time. The start time in the sumo time instant should be
        # the distance between midnight and the first ts in the previous list. Contrarily, the simulation will be at 0
        # even if the input dataset doesn't start at midnight
        ts_midnight = datetime.datetime.combine(ts[0], datetime.datetime.min.time())
        start_time_sec = np.abs((ts_midnight - ts[0]).total_seconds())
        ts_sumo = [start_time_sec + (ts_other - ts[0]).total_seconds() for ts_other in ts]

        data_df_formatted['ts'] = ts
        data_df_formatted.set_index('ts', inplace=True)

        cls.edge_mappings_dict[dataset_fname] = edge_mapping
        cls.ts_dict[dataset_fname] = ts
        cls.ts_sumo_dict[dataset_fname] = ts_sumo

        return data_df_formatted

    @classmethod
    def evaluate_missing_data_percentage(cls, df) -> DataFrame:
        missing_perc = {}
        cols = list(filter(lambda c: c != '-50', df.columns))
        for col in cols:
            count = len(df[df[col] == -50])
            missing_perc[col] = count / df[col].size

        return pd.DataFrame.from_dict([missing_perc]).T
