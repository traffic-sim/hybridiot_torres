from dataclasses import dataclass, field
from datetime import datetime


@dataclass(init=True)
class HybridIoT_Configuration:
    datetime_start: datetime = field(repr=False)  # the time when the samples for this config begin to be collected
    aggregation_interval_sec: int = field(repr=False)  # sampling interval in the input dataset in SECONDS
    perc_inactivity_sensors: int = field(repr=False)  # the simulated percentage of missing sample >per sensor<
    dataset_fname: str = field(repr=False)  # the training dataset
    test_dataset_fname: str = field(repr=False)  # the testing dataset
    additional: dict = field(repr=False)
    output_folder: str = field(repr=False, default='output/')
