import os
from datetime import datetime

from ExperimentsConfiguration.HybridIoT_Configuration import HybridIoT_Configuration

bxlmobilite_date_pattern = '%Y/%m/%d %H:%M'

exp_day63_by_minute = HybridIoT_Configuration(
    datetime_start=datetime.strptime('2023/03/07 09:12', bxlmobilite_date_pattern),
    aggregation_interval_sec=60,
    perc_inactivity_sensors=50,
    dataset_fname='dataset/BXLMobilite/dataset_day66.csv',
    test_dataset_fname='dataset/BXLMobilite/dataset_day67.csv',
    output_folder='output/dataset_day66_67/',
    additional={'sumo_net_path': os.environ['BXL_NET'],
                'sensors_pos_path': 'dataset/BXLMobilite/all_counting_devices_pos.csv'}

)
