from dataclasses import dataclass
from datetime import datetime

import Config
from ExperimentsConfiguration.HybridIoT_Configuration import HybridIoT_Configuration

exp_namur = HybridIoT_Configuration(
    datetime_start=datetime.strptime('2023/11/01 00:00', '%Y/%m/%d %H:%M'),
    aggregation_interval_sec=900,
    perc_inactivity_sensors=50,
    dataset_fname='dataset/Macq/dataset_resampled_2023-11-01_15min.parquet',
    test_dataset_fname='dataset/Macq/dataset_resampled_2023-11-02_15min.parquet',
    output_folder='output/2023-05-2and4_namur/',
    additional={}
)
