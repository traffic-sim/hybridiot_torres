import datetime
import os
from optparse import OptionParser

import pandas as pd
import tqdm
from pandas.core.frame import DataFrame


def get_data_from_date(data_df: DataFrame, date: datetime.date) -> DataFrame:
    """
    Return a dataset containing the data for the input date
    :param data_df:
    :param date:
    :return:
    """
    df_time = pd.to_datetime(data_df['timestamp'])
    data_df['ts'] = [timestamp.date() for timestamp in df_time.to_list()]

    return data_df.loc[data_df['ts'] == date]


def resample_dataset(data_df: DataFrame, resample_str: str = '30min') -> DataFrame:
    """
    Resample the input dataframe to the specified time interval
    :param data_df: a DATAFRAME
    :param resample_str:
    :return:
    """
    unique_edges = data_df.osm_id.unique()

    resampled_df = pd.DataFrame()
    for edge in tqdm.tqdm(unique_edges):
        xs = data_df.loc[data_df.osm_id == edge].copy()
        xs.drop(['speed_long', 'speed_short', 'time', 'osm_id', 'ts'], axis=1, inplace=True)
        xs.set_index('timestamp', inplace=True)
        rxs = xs.resample(resample_str).sum()
        rxs['osm_id'] = [edge for _ in range(len(rxs.index))]
        rxs['timestamp'] = rxs.index.to_list()
        rxs.reset_index(drop=True, inplace=True)
        resampled_df = pd.concat([resampled_df, rxs])
    return resampled_df


def get_available_days(data_df: DataFrame) -> set[datetime.date]:
    """
    Get the days for which there are measurements in the input dataframe
    :param data_df:
    :return:
    """
    df_time = pd.to_datetime(data_df['timestamp'])
    df_time.sort_values(inplace=True)
    return set([timestamp.date() for timestamp in df_time.to_list()])


def main_args(args=None):
    usage = "usage: python %prog [options] arg1 arg2\n\n\tExample usage: python %prog -o output/ -d dataset.parquet"
    opt_parser = OptionParser(usage=usage)
    opt_parser.add_option("-o", "--output_dir", dest="output_dir",
                          help="the FOLDER where the output (the scenario) is saved. Default: 'output/'")
    opt_parser.add_option("-d", "--dataset", dest="dataset", help="the PATH to the parquet dataset")

    if args is not None:
        (options, args) = opt_parser.parse_args(args)
    else:
        (options, args) = opt_parser.parse_args()

    output_dir = "output/" if not options.output_dir else options.output_dir

    if not options.dataset:
        opt_parser.print_help()
        opt_parser.error('Macq dataset file not provided')

    data_df = pd.read_parquet(options.dataset)

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    available_days = get_available_days(data_df)
    test_days = list(available_days)[:2]
    for day in test_days:
        print(f'# Processing dataset at {str(day)}')
        sub_df = get_data_from_date(data_df, day)
        sub_df.to_parquet(os.path.join(output_dir, f'dataset_resampled_{str(day)}_5min.parquet'))

        resampled_15min = resample_dataset(sub_df, '15min')
        resampled_15min.to_parquet(os.path.join(output_dir, f'dataset_resampled_{str(day)}_15min.parquet'))

        resampled_30min = resample_dataset(sub_df, '30min')
        resampled_30min.to_parquet(os.path.join(output_dir, f'dataset_resampled_{str(day)}_30min.parquet'))

        resampled_60min = resample_dataset(sub_df, '60min')
        resampled_60min.to_parquet(os.path.join(output_dir, f'dataset_resampled_{str(day)}_60min.parquet'))


if __name__ == '__main__':
    main_args()
