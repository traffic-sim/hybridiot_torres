import os
import random
import time
from threading import Lock

from DatasetConverter.MacqConverter import MacqConverter
from Estimation.HybridIoT import HybridIoT
from ExperimentsConfiguration import ExperimentsMacq
from ExperimentsConfiguration.HybridIoT_Configuration import HybridIoT_Configuration
from util import ResultsUtils
from util.ResultsUtils import HybridIoT_Result


def main_from_dataset(configuration: HybridIoT_Configuration, with_id=0):
    """
    Launch the experiment:
    1) Load the experiment configuration (input data, % of data to estimate)
    2) Estimate the data (2.1 simulate the lack of data, 2.2 estimate missing data)
    3) save the estimation results to Excel
    4) save the estimates in a CSV format for use with SUMO (with FlowRouter)

    :param configuration: the experiment configuration
    :param with_id: an ID for the experiment. All output files are marked with *.ID.csv/xlsx
    :return:
    """
    random.seed(time.time())
    my_lock = Lock()
    with my_lock:
        if not os.path.exists(configuration.output_folder):
            os.makedirs(configuration.output_folder, exist_ok=True)

    # *.parquet -> DataFrame
    var = {'net': os.environ['NAMUR_NET']}
    df_train = MacqConverter.to_dataframe(configuration.dataset_fname, var)
    df_test = MacqConverter.to_dataframe(configuration.test_dataset_fname, var)

    hybridIoT = HybridIoT()
    hybridIoT.train(df_train)
    hybridIoT.predict(df_test, percentage_unavailability=configuration.perc_inactivity_sensors)

    df_estimate = ResultsUtils.get_estimates_df(hybridIoT.agents)
    df_estimate.set_index(df_test.index, inplace=True)

    results = HybridIoT_Result(df_pred=df_estimate, df_true=df_test)

    ResultsUtils.save_estimation_results_to_excel(configuration,
                                                  results,
                                                  configuration.perc_inactivity_sensors,
                                                  my_lock, with_id)


if __name__ == '__main__':
    main_from_dataset(configuration=ExperimentsMacq.exp_namur, with_id=0)
