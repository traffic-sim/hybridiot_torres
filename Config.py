# HybridIoT configuration----------------------------------
CLEAR_FULL_BUFFERS = True

# Size of state windows assembled by sensing agents
DATAWINDOW_SIZE = 6

# minimum number of required data windows to pursue data estimation
NUM_DATAWINDOWS_FOR_ESTIMATION = 5

USE_ALL_CW_FOR_ESTIMATION = False

VERBOSE_LOG = False

DATA_BOOTSTRAP = 25 / 100
# ---------------------------------------------------------
