import pyproj

P = pyproj.Proj(proj='utm', zone=31, ellps='WGS84', preserve_units=True)  # 31 is france, belgium (mostly)
G = pyproj.Geod(ellps='WGS84')


def lonlat_to_xy(lon, lat) -> pyproj.Proj:
    return P(lon, lat)


def xy_to_lonlat(x, y) -> pyproj.Proj:
    return P(x, y, inverse=True)


def distance(Lat1, Lon1, Lat2, Lon2):
    return G.inv(Lon1, Lat1, Lon2, Lat2)[2]
