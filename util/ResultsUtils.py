import os
from dataclasses import dataclass
from pathlib import Path

import numpy as np
import pandas as pd
import plotly.graph_objects as go
from numpy import ndarray
from pandas.core.frame import DataFrame
from permetrics import RegressionMetric

import ExperimentsConfiguration.HybridIoT_Configuration
from DatasetConverter.DatasetConverter import DatasetConverter


@dataclass(init=True, repr=True)
class HybridIoT_Result:
    df_pred: pd.DataFrame
    df_true: pd.DataFrame
    ground_truth: ndarray = None
    estimates_mae: pd.DataFrame = None
    average_estimation_time: float = None


def to_sumo(converter: DatasetConverter,
            estimate_dataframe: DataFrame,
            configuration: ExperimentsConfiguration.HybridIoT_Configuration,
            var: dict,
            output_fname: str = 'sumo_calibration_input.csv'):
    df_test_speed = converter.to_dataframe(configuration.test_dataset_fname, var)
    converter.to_sumo_csv(configuration,
                          estimate_dataframe,
                          df_test_speed,
                          output_fname)


def get_estimates_df(agents):
    data_lengths = list(map(lambda ag: len(ag.data), agents))
    min_length = np.min(data_lengths)
    df_dict = {}
    for ag in agents:
        df_dict[ag.id] = ag.data[:min_length]
    return pd.DataFrame.from_dict(df_dict)


def plot_error(df_err, output_file=None):
    fig = go.Figure()
    fig.add_trace(
        go.Box(y=df_err["MAE"], x=list(df_err.index))
    )
    if output_file:
        fig.write_image("output_plot.pdf", width=750, scale=1.5)
    fig.show()


def write_output_series_to_excel(df_estimates, df_actual, output_excel_fname='output.xlsx'):
    abs_diff = pd.DataFrame(np.abs(df_estimates.to_numpy() - df_actual.to_numpy()))
    abs_diff.columns = df_estimates.columns

    df_time = pd.to_datetime(list(df_estimates.index)).tz_localize(None).to_series()
    df_time.sort_values(inplace=True)
    ts = df_time.to_list()
    df_estimates['ts'] = ts
    df_estimates.set_index('ts', inplace=True)
    df_actual['ts'] = ts
    df_actual.set_index('ts', inplace=True)
    diff_df = df_actual.subtract(df_estimates).abs()

    mape = {}
    nmrse = {}
    mae = {}
    for c in df_actual.columns:
        y_real = df_actual[c].to_list()
        y_sim = df_estimates[c].to_list()
        evaluator = RegressionMetric(y_real, y_sim, decimal=5)
        nmrse[c] = evaluator.normalized_root_mean_square_error()
        mae[c] = evaluator.mean_absolute_error()
        mape[c] = evaluator.mean_absolute_percentage_error()

    mape_df = pd.DataFrame.from_dict(mape, orient="index", columns=['mape'])
    mae_df = pd.DataFrame.from_dict(mae, orient="index", columns=['mae'])
    nmrse_df = pd.DataFrame.from_dict(nmrse, orient="index", columns=['nmrse'])
    df_errors = mape_df.join(mae_df).join(nmrse_df)

    writer = pd.ExcelWriter(output_excel_fname, mode="w")
    df_estimates.to_excel(writer, sheet_name='Estimated values')
    df_actual.to_excel(writer, sheet_name='Actual values')
    diff_df.to_excel(writer, sheet_name='Abs Difference (real-est)')
    df_errors.to_excel(writer, sheet_name='Errors')
    writer.close()


def save_estimation_results_to_excel(configuration: ExperimentsConfiguration.HybridIoT_Configuration,
                                     counts_results: HybridIoT_Result,
                                     perc_inactivity: int,
                                     my_lock,
                                     with_id: int = 0):
    dest_folder = os.path.join(configuration.output_folder,
                               f"missing_data_perc_{perc_inactivity}",
                               "counts")
    with my_lock:
        if not os.path.exists(dest_folder):
            os.makedirs(dest_folder, exist_ok=True)

    output_excel_counts_fname = os.path.join(dest_folder, f"counts_sim_id{with_id}.xlsx")

    write_output_series_to_excel(counts_results.df_pred,
                                 counts_results.df_true,
                                 str(output_excel_counts_fname))


def save_cumulative_mean_to_excel(
        configuration: ExperimentsConfiguration.HybridIoT_Configuration.HybridIoT_Configuration,
        perc_inactivity,
        errors,
        output_fname):
    dest_folder = os.path.join(configuration.output_folder,
                               f"missing_data_perc_{perc_inactivity}",
                               "errors_excel")

    pathlist = list(Path(dest_folder).rglob('*.xlsx'))
    columns = None

    all_errors_df = {}
    writer = pd.ExcelWriter(output_fname)
    for err in errors:
        cum_mae = {}
        for idx, path in enumerate(pathlist):
            df = pd.read_excel(path, sheet_name=err, index_col=0)
            if columns is None:
                columns = list(df.columns)
            err_values = []
            for interp_method in df.columns:
                err_values.append(np.nanmean(df[interp_method].to_numpy()))
            cum_mae[idx] = err_values
        err_df = pd.DataFrame.from_dict(cum_mae).T
        err_df.columns = columns

        all_errors_df[err] = err_df
        all_errors_df[f'cumulative_{err}'] = err_df.expanding().mean()
        err_df.to_excel(writer, sheet_name=err)
        err_df.expanding().mean().to_excel(writer, sheet_name=f'cumulative_{err}')
    writer.close()
