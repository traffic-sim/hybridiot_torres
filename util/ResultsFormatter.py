import pandas as pd
import os
import numpy as np
from pandas.io.excel._base import ExcelWriter
from openpyxl import load_workbook


def get_mean(fname, column):
    results = pd.read_excel(fname, sheet_name='Resume')
    return results[column].mean()


if __name__ == '__main__':
    xlsx_path = '/Users/dguastel/Desktop/dataset_day65/output_counts_Excel/'
    output_xlsx = 'resume_estimation_simpleImputer.xlsx'

    mape = {}
    mae = {}
    nmse = {}
    std = {}
    reached = []

    for file in os.listdir(xlsx_path):
        if file.endswith('.xlsx'):
            try:
                perc = int(file.split('_')[0][:2])
            except:
                continue
            if perc not in mape:
                print(f'working for {perc}%')
                mape[perc] = []
                mae[perc] = []
                nmse[perc] = []
                std[perc] = []
            mape[perc].append(get_mean("%s%s" % (xlsx_path, file), 'MAPE'))
            mae[perc].append(get_mean("%s%s" % (xlsx_path, file), 'MAE'))
            nmse[perc].append(get_mean("%s%s" % (xlsx_path, file), 'NMSE'))
            std[perc].append(get_mean("%s%s" % (xlsx_path, file), 'STDEV'))

    df_mape = pd.DataFrame.from_dict(mape)
    df_mae = pd.DataFrame.from_dict(mae)
    df_nmse = pd.DataFrame.from_dict(nmse)
    df_std = pd.DataFrame.from_dict(std)

    avg_mape = {}
    avg_mae = {}
    avg_nmse = {}
    avg_std = {}

    cum_avg_mae = {}
    cum_avg_nmse = {}
    cum_avg_mape = {}
    cum_avg_std = {}
    for perc in mape.keys():
        avg_mape[perc] = df_mape[perc].mean()
        avg_mae[perc] = df_mae[perc].mean()
        avg_nmse[perc] = df_nmse[perc].mean()
        avg_std[perc] = df_std[perc].mean()

        # calculate the cumulative mean
        num_exps = len(df_mape[perc])
        cum_avg_mae[perc] = [df_mae[perc][0]]
        cum_avg_mape[perc] = [df_mape[perc][0]]
        cum_avg_std[perc] = [df_std[perc][0]]
        cum_avg_nmse[perc] = [df_nmse[perc][0]]
        for i in range(1, num_exps):
            cum_avg_mae[perc].append(df_mae[perc][:i].mean())
            cum_avg_mape[perc].append(df_mape[perc][:i].mean())
            cum_avg_std[perc].append(df_std[perc][:i].mean())
            cum_avg_nmse[perc].append(df_nmse[perc][:i].mean())

    avg_df = pd.DataFrame([avg_mape, avg_nmse, avg_mae, avg_std]).T
    avg_df.columns = ['MAPE', 'NMSE', 'MAE', 'STD']
    avg_df = avg_df.T

    df_cum_mape = pd.DataFrame.from_dict(cum_avg_mape)
    df_cum_mae = pd.DataFrame.from_dict(cum_avg_mae)
    df_cum_nmse = pd.DataFrame.from_dict(cum_avg_nmse)
    df_cum_std = pd.DataFrame.from_dict(cum_avg_std)

    writer = pd.ExcelWriter(output_xlsx, mode="w")
    df_mape.to_excel(writer, sheet_name='MAPE')
    df_mae.to_excel(writer, sheet_name='MAE')
    df_nmse.to_excel(writer, sheet_name='NMSE')
    df_std.to_excel(writer, sheet_name='STD')
    avg_df.to_excel(writer, sheet_name='Resume')
    df_cum_mape.to_excel(writer, sheet_name='Cumulative MAPE')
    df_cum_mae.to_excel(writer, sheet_name='Cumulative MAE')
    df_cum_nmse.to_excel(writer, sheet_name='Cumulative NMSE')
    df_cum_std.to_excel(writer, sheet_name='Cumulative STD')

    writer.close()
