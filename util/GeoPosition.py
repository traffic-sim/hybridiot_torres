from util import GeodeticUtil


class GeoPosition:

    def __init__(self, longitude: float, latitude: float):
        self.lon = longitude
        self.lat = latitude

    def get_xy(self):
        return GeodeticUtil.lonlat_to_xy(self.lon, self.lat)

    def get_y(self):
        pass

    def __str__(self):
        return f"[Lon: {self.lon}, Lat: {self.lat}]"
