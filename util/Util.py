import random
from typing import List

import numpy as np

from Agent.DataWindow import DataWindow
from Agent.State import State
import pandas as pd


def get_random_ground_truth_mask(perc_boot_data, perc_inactivity_per_sensor, num_agents, num_samples):
    """
    Create an N*K ndarray, N is the number of ACA, K is the number of samples observed by ACAs
    Each column contains 1 indicating that the corresponding data is collected from a physical sensor.
    Contrarily, -1 indicates that the data must be estimated.
    :return: num_agents*num_samples ndarray
    """
    data_idx_to_not_estimate = int(num_samples * (perc_boot_data / 100))
    mask = np.ones([num_samples, num_agents], dtype=int)
    for ag in range(num_agents):
        # idx_to_estimate = list(range(1, num_samples - 1))
        idx_to_estimate = list(range(data_idx_to_not_estimate + 1, num_samples - 1))
        # [idx_to_estimate.pop(v) for v in range(data_idx_to_not_estimate)]
        random.shuffle(idx_to_estimate)
        idx_to_estimate = idx_to_estimate[: int(len(idx_to_estimate) * (perc_inactivity_per_sensor / 100))]

        mask[idx_to_estimate, [ag]] = -1
    return mask


def get_ground_truth_mask_from_data(data_df, to_estimate_value=-1, not_to_estimate_value=1):
    return data_df.mask(lambda x: np.isfinite(x), other=not_to_estimate_value).mask(lambda x: np.isnan(x),
                                                                                    other=to_estimate_value).to_numpy().astype(
        int)


def get_ground_truth_mask_from(nd_mask, perc_inactivity_per_sensor, num_agents, num_samples):
    """
    Create an N*K ndarray, N is the number of ACA, K is the number of samples observed by ACAs
    Each column contains 1 indicating that the corresponding data is collected from a physical sensor.
    Contrarily, -1 indicates that the data must be estimated.
    :param gf: the initial gf with -1 or 1
    :return: num_agents*num_samples ndarray
    """
    for col in nd_mask.shape[0]:
        idx_good = np.argwhere(nd_mask[:, col] == 0)

    mask = np.ones([num_samples, num_agents], dtype=int)
    for ag in range(num_agents):
        idx_to_estimate = list(range(num_samples))
        random.shuffle(idx_to_estimate)
        idx_to_estimate = idx_to_estimate[: int(len(idx_to_estimate) * (perc_inactivity_per_sensor / 100))]
        mask[idx_to_estimate, [ag]] = -1
    return mask


def pad_buffer(buffer: List[State], series: List[DataWindow], w_size: int):
    """Return a padded version of the input buffer. In the output buffer, each list of floating car
     data is filled with elements from previously observed series so to obtain lists of size w_size
    :param buffer: a list of State
    :param series: a list of StateWindow
    :param w_size: the size that each buffer (that is, each buffer for information type) must have
    :return: the padded buffer, containing lists of size w_size
    """
    last_index = len(series) - 1
    s = series[last_index]  # convert the last observed series to array
    ptr_series = len(s.states) - 1  # index of the last TrafficState of s
    while len(buffer) < w_size:  # loop until buf reaches size w_size
        buffer.insert(0, s.states[ptr_series])  # insert elements from s (in a backward manner)
        ptr_series = ptr_series - 1  # go to previous element of s
        if ptr_series < 0:  # if we added all the elements of s,
            last_index = last_index - 1  # then move to the previous series
            if last_index < 0:  # if no more series available, raise an exception
                raise Exception("No more series to lookup for padding buffer.")
            s = series[last_index]  # prepare for next iteration
            ptr_series = len(s.states) - 1  # prepare for next iteration


def unroll_real_or_estimate_data_in_state(list_states: List[State]):
    """Unroll a list of State object to a list of float/integer """
    return list(map(lambda x: x.data, list_states))


def HybridIoT_distance(series_1: np.array, series_2: np.array):
    """
    Calculate the distance between two numerical series
    :return: a float value indicating the distance
    """
    return np.sum(np.abs(series_1 - series_2)) / series_1.size


def HybridIoT_distance_series(series_1: pd.Series, series_2: pd.Series):
    """
    Calculate the distance between two numerical series
    :return: a float value indicating the distance
    """
    return (series_1.sub(series_2).abs().sum()) / series_1.size

    # return np.sum(np.abs(series_1 - series_2)) / series_1.size
