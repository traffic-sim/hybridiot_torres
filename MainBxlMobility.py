import os
import random
import time
from threading import Lock

from DatasetConverter.BXLMobilityConverter import BXLMobilityConverter
from Estimation.HybridIoT import HybridIoT
from ExperimentsConfiguration import ExperimentsBXLMobilite
from ExperimentsConfiguration.HybridIoT_Configuration import HybridIoT_Configuration
from util import ResultsUtils
from util.ResultsUtils import HybridIoT_Result


def main_from_dataset(configuration: HybridIoT_Configuration, with_id=0):
    random.seed(time.time())
    my_lock = Lock()
    with my_lock:
        if not os.path.exists(configuration.output_folder):
            os.makedirs(configuration.output_folder, exist_ok=True)

    # *.parquet -> DataFrame
    var = {'net': configuration.additional['sumo_net_path'],
           'sensors_pos_path': configuration.additional['sensors_pos_path']}
    df_train = BXLMobilityConverter.to_dataframe(configuration.dataset_fname, var)
    df_test = BXLMobilityConverter.to_dataframe(configuration.test_dataset_fname, var)

    # imputation
    hybridIoT = HybridIoT()
    hybridIoT.train(df_train)
    hybridIoT.predict(df_test, percentage_unavailability=configuration.perc_inactivity_sensors)

    # DataFrame with estimated data
    df_estimate = ResultsUtils.get_estimates_df(hybridIoT.agents)
    df_estimate.set_index(df_test.index, inplace=True)

    results = HybridIoT_Result(df_pred=df_estimate, df_true=df_test)

    # to SUMO csv file (for use with RouteSampeler)
    var = {'net': configuration.additional['sumo_net_path'],
           'sensors_pos_path': configuration.additional['sensors_pos_path'],
           'speed': 0}

    ResultsUtils.to_sumo(BXLMobilityConverter(), df_estimate, configuration, var)

    # save results (true/pred/errors) to Excel
    ResultsUtils.save_estimation_results_to_excel(configuration,
                                                  results,
                                                  configuration.perc_inactivity_sensors,
                                                  my_lock, with_id)


if __name__ == '__main__':
    main_from_dataset(ExperimentsBXLMobilite.exp_day63_by_minute, with_id=0)
