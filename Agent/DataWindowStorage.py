import itertools
from typing import Tuple
import numpy as np
from pandas.core.series import Series

from Agent.DataWindow import DataWindow


class DataWindowStorage:

    def __init__(self):
        self.num_windows = 0
        self.storage = {}  # type: dict[Tuple,list[DataWindow]]
        self.centroids = {}  # type: dict[Tuple, float]

    def get_all(self):
        return list(itertools.chain(*list(self.storage.values())))

    def add(self, a: DataWindow):
        added = False
        for k in self.storage:
            if k[0] <= a.data.min() <= a.data.max() <= k[1]:
                self.storage[k].append(a)
                added = True

        if not added:
            self.storage[(a.data.min(), a.data.max())] = [a]
            self.centroids[(a.data.min(), a.data.max())] = (a.data.min() + a.data.max()) / 2
        self.num_windows += 1

    def get_cluster(self, a: DataWindow) -> list[DataWindow]:
        for k in self.storage:
            if k[0] <= a.data.min() <= a.data.max() <= k[1]:
                return self.storage[k]
        return []

    def get_closest_cluster(self, a: Series, exclude=None) -> Tuple[float, float] | None:
        if exclude is None:
            exclude = []

        window_centroid = (a.min() + a.max()) / 2
        kk = {}
        for k in self.centroids:
            kk[k] = np.abs(self.centroids[k] - window_centroid)
        kk = dict(sorted(kk.items(), key=lambda item: item[1]))

        for edges in kk:
            if edges not in exclude:
                return edges
        return None

    def get_most_similar(self, a: Series, n: int) -> list[Tuple[DataWindow, float]]:
        """
        get n most similar data windows to a
        :param a:
        :param n:
        :return:
        """
        output_list = []
        analyzed_clusters = []

        while len(analyzed_clusters) < len(self.centroids):
            closest_cluster = self.get_closest_cluster(a, analyzed_clusters)
            analyzed_clusters.append(closest_cluster)
            for w in self.storage[closest_cluster]:
                # dist = HybridIoT_distance_series(a, w.data)

                dist = (a.sub(w.data).abs().sum()) / a.size

                output_list.append((w, dist))
                if len(output_list) >= n:
                    return output_list
        return output_list
