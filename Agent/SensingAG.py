import random
import sys
import time
from copy import deepcopy
from multiprocessing.pool import ThreadPool
from typing import Dict

import numpy as np

import Config
from Agent.DataWindow import DataWindow
from Agent.DataWindowStorage import DataWindowStorage
from Agent.State import State
from Estimation.EstimationResult import EstimationResult
from Estimation.EstimationStrategy import EstimationStrategy
from util.GeoPosition import GeoPosition


class SensingAG:
    # for exploring strategies
    eps = 0.2

    def __init__(self, agent_id: str, position: GeoPosition, type_ag: str, data):
        self.id = agent_id
        self.position = position  # type: GeoPosition
        self.type_ag = type_ag  # type: str
        self.state_buffer = []  # type: list[State]
        self.states_windows = []  # type: list[DataWindow]

        # key: ID of strategy, value: LIST of CW
        self.states_windows_by_strategy = {}
        # key: ID of strategy, value: LIST of CW
        self.data_by_strategy = {}
        self.state_buffer_by_strategy = {}

        if data is None:
            data = []
        self.oracle_data = data

        # the data perceived or estimated by the sensing agent
        self.data = []

        self.neighbor_agents = []  # type: list[SensingAG]
        self.neighbor_agents_confidence = {}  # type: dict[str, float]
        # a ndarray containing as elements as the data observed by this agent.
        # -1 indicates that the data must be estimated, 1 when sensor is alive
        self.ground_truth_mask = None

        # key: ID of a strategy, value: LIST of SCORES (for the moment of size 10)
        self.last_strategies_scores = {}
        # f(t) = ID, t time instant, ID the identifier of the best estimation strategy at time t
        self.best_strategy_hist = []

        # if True, the new data window is added to the historic
        self.add_datawindow_on_estimate = True

        self.estimation_strategies = []

        self.last_data_window = None
        self.last_data_windows = []

        self.data_storage_by_strategy = {}  # type: dict[int,DataWindowStorage]

    def add_estimation_strategy(self, strategy: EstimationStrategy):
        self.estimation_strategies.append(strategy)
        strategy_id = strategy.id
        self.data_by_strategy[strategy_id] = []
        self.state_buffer_by_strategy[strategy_id] = []

        self.last_strategies_scores[strategy_id] = []

        self.data_storage_by_strategy[strategy_id] = DataWindowStorage()

    def set_add_datawindow_on_estimate(self, b: bool):
        self.add_datawindow_on_estimate = b

    def set_data(self, data):
        self.oracle_data = data
        self.data.clear()

    def set_ground_truth(self, data):
        self.ground_truth_mask = data

    def add_neighbor_agent(self, ag):
        self.neighbor_agents.append(ag)
        self.neighbor_agents_confidence[ag] = 0.0

    def __str__(self):
        return f"AG: {self.id}, type_ag: {self.type_ag},\ndata: {self.oracle_data}"

    def on_simstep_performed(self, time_inst, is_prediction=False) -> float:
        """
        Operation(s) to perform at each time step
        :param time_inst: Allows to know when the agent has perceived information
        :return: the elapsed time (float)
        """
        start = time.time()
        result = self.perceive_and_act(time_inst, is_prediction)
        self.save_traffic_state(result.new_state, result.strategy, is_prediction)
        end = time.time()
        return end - start

    def get_best_strategy_estimate(self, strategy_output: Dict[int, EstimationResult]) -> EstimationResult:
        """
        Choose the estimate that is closest to the real value. Perform a dummy RL update to keep track of the best
        strategy

        :param strategy_output: a dict where key is strategy ID, value is EstimationResult
        :return: an EstimationResult object generated from the best estimation strategy
        """
        if random.random() < SensingAG.eps:
            # random choice
            best_strategy_output = strategy_output[random.choice(list(strategy_output.keys()))]
            self.data.append(best_strategy_output.estimate)
            return best_strategy_output

        # else, do the exploration
        best_score = 0  # as the score is calculated as 1/error, here the score must be maximized
        best_strategy_id = 1
        # update the scores
        for strategy_id in strategy_output.keys():
            est_result = strategy_output[strategy_id]
            self.last_strategies_scores[strategy_id].append(est_result.score)
            while len(self.last_strategies_scores[strategy_id]) > 10:  # TODO why 10 ?
                self.last_strategies_scores[strategy_id].pop(0)
            # calculate the avg score for this strategy
            # keep the 'best' score
            avg_score = np.mean(self.last_strategies_scores[strategy_id])
            if avg_score > best_score:
                best_score = avg_score
                best_strategy_id = strategy_id
        best_strategy_output = strategy_output[best_strategy_id]
        self.best_strategy_hist.append(best_strategy_id)
        self.data.append(best_strategy_output.estimate)
        return best_strategy_output

    def estimate_value(self, strategy: EstimationStrategy, time_instant: int, is_prediction=False) -> EstimationResult:

        if is_prediction:
            value = strategy.estimator.estimate_data(time_instant,
                                                     self,
                                                     self.state_buffer_by_strategy[strategy.id],
                                                     strategy.num_windows_for_estimation,
                                                     strategy.data_window_size,
                                                     strategy.id,
                                                     self.ground_truth_mask)
        else:
            value = None
            strategy.estimator.calculate_agent_confidence(self,
                                                          self.state_buffer_by_strategy[strategy.id],
                                                          strategy.num_windows_for_estimation,
                                                          strategy.data_window_size,
                                                          strategy.id)

        if value is None:
            # estimation didn't succeed. Maybe because sensor is alive at current time. Use oracle data then
            self.data_by_strategy[strategy.id].append(self.oracle_data[time_instant])
            return EstimationResult(self.oracle_data[time_instant], strategy, 0,
                                    State(self.id, self.oracle_data[time_instant], time_instant,
                                          is_estimate=True))

        self.data_by_strategy[strategy.id].append(value)
        if self.oracle_data[time_instant] - value != 0:
            # strategy score
            score = 1 / np.abs(self.oracle_data[time_instant] - value)
        else:
            score = sys.float_info.max

        return EstimationResult(value, strategy, score,
                                State(self.id, value, time_instant, is_estimate=True))

    def perceive_and_act(self, time_instant, is_prediction=False) -> EstimationResult:
        """
        Perceive the environment through the physical sensor (if available) otherwise do estimation

        :param time_instant: the current time instant
        :return: A State Object
        """
        strategy_output = {}

        pool = ThreadPool(len(self.estimation_strategies))
        results = pool.starmap(self.estimate_value,
                               [(strategy, time_instant, is_prediction) for strategy in self.estimation_strategies])
        pool.close()
        pool.join()
        for r in results:
            strategy_output[r.strategy.id] = r

        # for strategy in self.estimation_strategies:
        #     strategy_output[strategy.id] = self.estimate_value(strategy, time_instant, is_prediction)

        return self.get_best_strategy_estimate(strategy_output)

    def save_traffic_state(self, state: State, strategy: EstimationStrategy, is_prediction=False):
        """
        Save the input traffic state in the state buffer of this agent. If this buffer is full, then a new data window
        is created and added to the knowledge base
        :param strategy:
        :param state: An object State
        """

        if len(self.state_buffer_by_strategy[strategy.id]) < strategy.data_window_size:
            self.state_buffer_by_strategy[strategy.id].append(state)
            return

        if not is_prediction:
            if self.add_datawindow_on_estimate:
                data_window = DataWindow(deepcopy(self.state_buffer_by_strategy[strategy.id]))

                self.last_data_window = data_window
                self.last_data_windows.append(data_window)
                while len(self.last_data_windows) > 2:
                    self.last_data_windows.pop(0)

                self.data_storage_by_strategy[strategy.id].add(data_window)

                # self.states_windows_by_strategy[strategy.id].append(data_window)
                # print(f'AG: {self.id} has #{len(self.states_windows_by_strategy[strategy.id])} windows')
        if Config.CLEAR_FULL_BUFFERS:
            self.state_buffer_by_strategy[strategy.id].clear()
            self.state_buffer_by_strategy[strategy.id].append(state)
        else:
            del self.state_buffer_by_strategy[strategy.id][0]  # remove first element
            self.state_buffer_by_strategy[strategy.id].append(state)

    def clear_buffer(self):
        [self.state_buffer_by_strategy[strategy.id].clear() for strategy in self.estimation_strategies]
