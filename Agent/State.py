class State:

    def __init__(self, sensing_id: str, data: float = 0.0, time: int = 0, is_estimate: bool = False):
        self.data = data  # data perceived or estimated
        self.time = time
        self.is_estimate = is_estimate  # True if the value of this state has been estimated
        self.sensing_id = sensing_id  # the sensing agent that owns this state
        self.most_similar_windows_ref = []  # list index the most similar StateWindow

    def __repr__(self):
        return f"State[data: {self.data} estimate: {self.is_estimate} sensing_id: {self.sensing_id} at time: {self.time}]"
