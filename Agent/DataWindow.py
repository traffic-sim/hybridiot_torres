from pandas.core.series import Series

from Agent.State import State
import pandas as pd


class DataWindow:

    def __init__(self, states, id=0):
        self.states = states  # type: list[State]

        self.data = pd.Series(list(map(lambda s: s.data, states)))  # type: Series
        self.is_estimate = list(map(lambda s: s.is_estimate, states))

        self.id = id

    def get_most_similar_windows_id(self):
        """Return the IDs of the most similar StateWindow. These windows are those that contributed to
        the estimation of the environmental information for the last state of this window (that is, the representative
        element of this window)
        :return: a list of integer"""
        return self.states[-1].most_similar_windows_ref

    def __repr__(self):
        return f"Data Window [id:{id(self)}, states: {self.states}"
