import copy
from typing import Tuple

import numpy as np

import Config
from Agent.DataWindow import DataWindow
from Estimation.Estimator import Estimator
from util import Util, GeodeticUtil


def pairwise(data):
    return zip(data[::2], data[1::2])


class EstimatorConfidenceZone(Estimator):

    @classmethod
    def ag_in_confidence_zone(cls, agents):
        """
        Return the agents in the confidence zone
        :param agents:
        :return:
        """
        # TODO

        pass

    @classmethod
    def get_collinearity(cls, ag1, ag2, ag3):
        x1, y1 = ag1.position.get_xy()
        x2, y2 = ag2.position.get_xy()
        x3, y3 = ag3.position.get_xy()
        return x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)

    @classmethod
    def get_candidate_ag_pairs(cls, myself, ag_in_cz):
        """
        Return the pairs of agents. Agents are sorted according to their distance to myself

        :param myself: the agent that must estimate the missing data
        :param ag_in_cz: the list of agents inside the confidence zone of myself
        :return:
        """
        positions = {myself: myself.position.get_xy()}
        distances = {}
        # Step 2. Evaluate agents pairs
        # Step 2.1 get agents' position
        for ag in ag_in_cz:
            # each value is a pair (X,Y) of type (FLOAT,FLOAT)
            positions[ag] = ag.position.get_xy()
            distances[ag] = GeodeticUtil.distance(myself.position.lat,
                                                  myself.position.lon,
                                                  ag.position.lat,
                                                  ag.position.lon)

        # Agents sorted according to the distance to myself
        return pairwise(sorted(distances))

    @classmethod
    def calculate_data_fields(cls, myself, sorted_ag_pairs):
        """
        A data field is an estimate provided by a pair of agents

        :param myself: the agent that must estimate a missing value
        :param sorted_ag_pairs: list of pairs of agents, sorted according to the distance to myself
        :return:
        """
        collinearities = []
        data_fields = []
        for ag_pair in sorted_ag_pairs:
            v_ag0 = ag_pair[0].data[-1]
            v_ag2 = ag_pair[1].data[-1]
            dist_ag0 = GeodeticUtil.distance(myself.position.lat,
                                             myself.position.lon,
                                             ag_pair[0].position.lat,
                                             ag_pair[0].position.lon)
            dist_ag02 = GeodeticUtil.distance(ag_pair[0].position.lat,
                                              ag_pair[0].position.lon,
                                              ag_pair[1].position.lat,
                                              ag_pair[1].position.lon)
            data_fields.append(v_ag0 + ((v_ag2 - v_ag0) * (dist_ag0 / dist_ag02)))
            collinearities.append(cls.get_collinearity(ag_pair[0], myself, ag_pair[1]))
        return data_fields, collinearities

    @classmethod
    def calculate_estimation_in_confidence_zone(cls, myself, ag_in_cz):
        # Step 1. get agents' perceptions
        agents_perceptions = {}
        for ag in ag_in_cz:
            if ag.data:
                agents_perceptions[ag] = ag.data[-1]

        # Step 2. get the agents pairs
        sorted_ag_pairs = cls.get_candidate_ag_pairs(myself, ag_in_cz)

        # Step 3. Calculate the data fields and the collinearity between pairs
        data_fields, collinearities = cls.calculate_data_fields(myself, sorted_ag_pairs)

        # Step 4. calculate the estimate
        return np.average(data_fields, weights=collinearities)

    @classmethod
    def estimate_data(cls, time, sensing_ag, query_buffer, num_data_windows_for_estimation,
                      datawindow_size, strategy_id, ground_truth_mask) -> float | None:
        """
        Estimate the data at [time] starting from query buffer, which is the last
        observed  windows state. The estimation is performed by a technique (REF PLEASE)
        :param time: the current time instant
        :param sensing_ag: SensingAG object
        :param query_buffer: The actual buffer of the agent
        :param num_data_windows_for_estimation: the number of data windows to use for the estimation
        :param datawindow_size: the size of the data windows
        :param strategy_id: the ID of the strategy used for estimating missing data
        :param ground_truth_mask: a ndarray. Columns are sensors, rows is time. 1 means sensor is alive, -1 estimate
        :return: The estimated data (float)
        :return: The estimated data
        """
        sensor_alive = True if ground_truth_mask[time] > 0 else False
        datawindows_historic = sensing_ag.data_storage_by_strategy[strategy_id].get_all()
        if (len(datawindows_historic) < num_data_windows_for_estimation) or sensor_alive:
            if Config.VERBOSE_LOG:
                print(
                    f"[AG #{sensing_ag.id}] Not enough windows, or sensor is alive. Not estimating at #{time}.")
            return None
        # pad to obtain a homogeneous buffer where all lists have size config.data_window_size
        query_buffer_copy = copy.deepcopy(query_buffer)
        Util.pad_buffer(query_buffer_copy, datawindows_historic, datawindow_size)
        # query_array is  a vector of  data
        # >> map the list of States to numpy array (containing densities)
        query_array = np.array(Util.unroll_real_or_estimate_data_in_state(query_buffer_copy))

        cls.calculate_estimation_in_confidence_zone(sensing_ag, sensing_ag.neighbor_agents)

        exo_estimates = []
        for other_ag in sensing_ag.neighbor_agents:
            if not other_ag.states_windows_by_strategy[strategy_id.id]:
                continue
            windows_other_ags = cls.get_most_similar_data_windows(query_array,
                                                                  other_ag.states_windows_by_strategy[strategy_id],
                                                                  num_data_windows=num_data_windows_for_estimation)
            num, den = cls.calculate_estimation_weight(windows_other_ags)
            if (not np.isfinite(den)) or den == 0:
                continue
            exo_estimates.append(query_array[-1] + (num / den))
        the_estimate = np.nanmean(exo_estimates)
        return the_estimate
