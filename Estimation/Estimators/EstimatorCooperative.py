import copy
import random
import sys
from typing import Tuple

import numpy as np
import pandas as pd
import Config
from Agent.DataWindow import DataWindow
from Estimation.Estimator import Estimator
from util import Util
from heapq import nlargest


class EstimatorCooperative(Estimator):

    @classmethod
    def estimate_data(cls, time, sensing_ag, query_buffer, num_data_windows_for_estimation,
                      datawindow_size, strategy_id, ground_truth_mask) -> float | None:
        """
        Estimate the data at [time] starting from query buffer, which is the last
        observed  windows state. The estimation is performed by a technique (REF PLEASE)
        :param time: the current time instant
        :param sensing_ag: SensingAG object
        :param query_buffer: The actual buffer of the agent
        :param num_data_windows_for_estimation: the number of data windows to use for the estimation
        :param datawindow_size: the size of the data windows
        :param strategy_id: the ID of the strategy used for estimating missing data
        :param ground_truth_mask: a ndarray. Columns are sensors, rows is time. 1 means sensor is alive, -1 estimate
        :return: The estimated data (float)
        """
        sensor_alive = True if ground_truth_mask[time] > 0 else False
        num_windows = sensing_ag.data_storage_by_strategy[strategy_id].num_windows
        if num_windows < num_data_windows_for_estimation or sensor_alive:
            if Config.VERBOSE_LOG:
                print(
                    f"[AG #{sensing_ag.id}] Not enough windows, or sensor is alive. Not estimating at #{time}.")
            return None

        # pad to obtain a homogeneous buffer where all lists have size config.data_window_size
        query_buffer_copy = copy.deepcopy(query_buffer)
        Util.pad_buffer(query_buffer_copy, sensing_ag.last_data_windows, datawindow_size)

        # query_array is  a vector of  data
        # >> map the list of States to numpy array (containing  densities)
        query_array = pd.Series(Util.unroll_real_or_estimate_data_in_state(query_buffer_copy))

        exo_estimates = []

        # for other_ag in sensing_ag.neighbor_agents:
        num_ags = int(len(sensing_ag.neighbor_agents) * 0.1)
        best_ags = nlargest(num_ags, sensing_ag.neighbor_agents_confidence,
                            key=sensing_ag.neighbor_agents_confidence.get)

        for other_ag in best_ags:
            windows_other_ags = other_ag.data_storage_by_strategy[strategy_id].get_most_similar(query_array,
                                                                                                num_data_windows_for_estimation)  # type: list[Tuple[DataWindow, float]]

            num, den = cls.calculate_estimation_weight(windows_other_ags)
            if (not np.isfinite(den)) or den == 0:
                continue

            exo_estimates.append(query_array.iat[-1] + (num / den))
        if len(exo_estimates) > 0:
            return np.nanmean(exo_estimates)
        return None

    @classmethod
    def calculate_agent_confidence(cls, sensing_ag,
                                   query_buffer,
                                   num_data_windows_for_estimation,
                                   datawindow_size, strategy_id):
        num_windows = sensing_ag.data_storage_by_strategy[strategy_id].num_windows
        if num_windows < num_data_windows_for_estimation:  # or len(sensing_ag.last_data_windows) < 2:
            return

        # pad to obtain a homogeneous buffer where all lists have size config.data_window_size
        query_buffer_copy = copy.deepcopy(query_buffer)
        Util.pad_buffer(query_buffer_copy, sensing_ag.last_data_windows, datawindow_size)
        # query_array is  a vector of  data
        # >> map the list of States to numpy array (containing  densities)
        query_array = pd.Series(Util.unroll_real_or_estimate_data_in_state(query_buffer_copy))
        ag_to_cooperate = int(len(sensing_ag.neighbor_agents) * 0.1)
        if pd.Series(sensing_ag.neighbor_agents_confidence.values()).sum() > 0:
            the_agents = random.choices(list(sensing_ag.neighbor_agents_confidence.keys()),
                                        weights=sensing_ag.neighbor_agents_confidence.values(),
                                        k=ag_to_cooperate)
        else:
            the_agents = random.choices(list(sensing_ag.neighbor_agents_confidence.keys()),
                                        k=ag_to_cooperate)

        min_dist = sys.float_info.max
        most_confident_agent = None
        for other_ag in the_agents:
            if other_ag.last_data_window is None:
                continue
            dist = (query_array.sub(other_ag.last_data_window.data).abs().sum()) / query_array.size

            if dist < min_dist:
                min_dist = dist
                most_confident_agent = other_ag

        if most_confident_agent is not None:
            sensing_ag.neighbor_agents_confidence[most_confident_agent] += 1
