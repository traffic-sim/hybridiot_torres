import copy

import numpy as np

import Config
from Estimation.Estimator import Estimator
from util import Util


class EstimatorNonCooperative(Estimator):

    @classmethod
    def estimate_data(cls, time, sensing_ag, query_buffer, num_data_windows_for_estimation,
                      datawindow_size, strategy_id, ground_truth_mask) -> float | None:
        """
        Estimate the data at [time] starting from query buffer, which is the last
        observed  windows state. The estimation is performed by a technique (REF PLEASE)
        :param time: the current time instant
        :param sensing_ag: SensingAG object
        :param query_buffer: The actual buffer of the agent
        #:param datawindows_historic: The list of Data Windows to use for estimating the missing data
        :param num_data_windows_for_estimation: the number of data windows to use for the estimation
        :param datawindow_size: the size of the data windows
        :param strategy_id: the ID of the strategy used for estimating missing data
        :param ground_truth_mask: a ndarray. Columns are sensors, rows is time. 1 means sensor is alive, -1 estimate
        :return: The estimated data (float)
        """
        sensor_alive = True if ground_truth_mask[time] > 0 else False
        # num_data_windows_for_estimation = strategy.num_windows_for_estimation
        # datawindow_size = strategy.data_window_size
        datawindows_historic = sensing_ag.data_storage_by_strategy[strategy_id].get_all()
        if (len(datawindows_historic) < num_data_windows_for_estimation) or sensor_alive:
            if Config.VERBOSE_LOG:
                print(
                    f"[AG #{sensing_ag.id}] Not enough windows, or sensor is alive. Not estimating at #{time}.")
            return None

        # pad to obtain a homogeneous buffer where all lists have size config.data_window_size
        query_buffer_copy = copy.deepcopy(query_buffer)
        Util.pad_buffer(query_buffer_copy, datawindows_historic, datawindow_size)
        # query_array is  a vector of  data
        # >> map the list of States to numpy array (containing  densities)
        query_array = np.array(Util.unroll_real_or_estimate_data_in_state(query_buffer_copy))

        # windows_endogenous is a list of tuples (DataWindow, float) where float
        # is the similarity score, that is, the distance between query array and the most
        # similar context window in THIS agent's knowledge base
        windows_endogenous = cls.get_most_similar_data_windows(query_array, datawindows_historic,
                                                               num_data_windows_for_estimation)

        # Calculate the weight to be used for estimating the data
        # (ref: Eqs 2-3 https://doi.org/10.1109/ACCESS.2020.3028967)
        num, den = cls.calculate_estimation_weight(windows_endogenous)
        if (not np.isfinite(den)) or den == 0:
            return None

        estimation = query_array[-1] + (num / den)
        return estimation
