import tqdm
from numpy import ndarray
from pandas.core.frame import DataFrame

from Agent.SensingAG import SensingAG
from Estimation.EstimationStrategy import EstimationStrategy
from Estimation.Estimators.EstimatorCooperative import EstimatorCooperative
from util import Util
from util.GeoPosition import GeoPosition


class HybridIoT:
    agents: list[SensingAG] = []

    def __init__(self):
        pass

    @classmethod
    def train(cls, data_df: DataFrame):
        """
        Create HybridIoT agents and train them on input data

        :param data_df:
        :return:
        """
        ground_truth = Util.get_ground_truth_mask_from_data(data_df, 1, 1)
        cls.create_agents(data_df, ground_truth)
        cls.do_simulation(len(data_df.index))

    @classmethod
    def predict_horizon(cls, dataset_df, horizon_str="2H"):
        # TODO !
        pass

    @classmethod
    def predict(cls, dataset_df: DataFrame, percentage_unavailability: int = 50):
        [ag.clear_buffer() for ag in cls.agents]
        [ag.set_add_datawindow_on_estimate(False) for ag in cls.agents]

        ground_truth = Util.get_random_ground_truth_mask(0,
                                                         percentage_unavailability,
                                                         len(dataset_df.columns),  # num sensors
                                                         len(dataset_df.index))  # num data

        for idx, ag in enumerate(cls.agents):
            ag.set_data(dataset_df.loc[:, ag.id].tolist())
            ag.set_ground_truth(ground_truth[:, idx])

        cls.do_simulation(len(dataset_df.index), True)

    @classmethod
    def create_agents(cls, dataset_df: DataFrame, ground_truth: ndarray = None):
        """
        Create HybridIoT agents. Each agent is associated with a sensor (column in dataset_df)

        :param dataset_df: the dataset in DATAFRAME
        :param ground_truth: NDARRAY. Same size as dataset_df. 1 if the data is available, -1 otherwise. This is to
        simulate the sensors intermittence
        :return:
        """
        for i, col_name in enumerate(list(dataset_df.columns)):
            ag = SensingAG(col_name,
                           GeoPosition(0, 0),
                           "ag",
                           dataset_df.iloc[:, i].tolist())
            if ground_truth is not None:
                ag.ground_truth_mask = ground_truth[:, i]

            estimation_strategies = [
                EstimationStrategy(1, EstimatorCooperative(), "coop_4windows_Wsize8", 4, 8, 0.1),
                #EstimationStrategy(2, EstimatorCooperative(), "coop_4windows_Wsize6", 4, 6, 0.1),
                EstimationStrategy(2, EstimatorCooperative(), "coop_4windows_Wsize4", 4, 4, 0.1)
            ]

            [ag.add_estimation_strategy(st) for st in estimation_strategies]

            cls.agents.append(ag)

        for ag in cls.agents:
            other_agents = list(filter(lambda agent: agent.id != ag.id, cls.agents))
            [ag.add_neighbor_agent(other_agent) for other_agent in other_agents]

    @classmethod
    def do_simulation(cls, num_dataset_sample, is_prediction=False) -> list[float]:
        """
        Run HybridIoT agents to impute missing values

        :param num_dataset_sample: total number of samples in the input dataset
        :return: the elapsed time of each agent
        """
        elapsed_time = []
        for t in tqdm.tqdm(range(num_dataset_sample)):
            for ag in cls.agents:
                time_elapsed = ag.on_simstep_performed(t, is_prediction)
                elapsed_time.append(time_elapsed)
        return elapsed_time
