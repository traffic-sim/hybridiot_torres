from typing import Tuple

from Agent.DataWindow import DataWindow
from util import Util
import numpy as np


class Estimator:

    @classmethod
    def get_most_similar_data_windows(cls, query_array, historic, num_data_windows) -> list[Tuple[DataWindow, float]]:
        """ Given a query_array (a ndarray containing data),
        return the most N similar windows in historic

        :param query_array: data window containing the data to estimate (ndarray object)
        :param historic: a list of data windows
        :param num_data_windows: the number of data windows to get
        :return: a list of tuples (StateWindow, float), where the
        float value is the distance between StateWindow and the query_array.\n
        Please note that the distance is calculated according to data values"""

        # map the  states windows to a list of tuples (StateWindow, float) where
        # float is Distance(DataWindow_i,query_array)
        windows_distances = list(
            map(lambda x: (x,
                           Util.HybridIoT_distance(np.array(Util.unroll_real_or_estimate_data_in_state(x.states)),
                                                   query_array)
                           ), historic))

        # value in windows_distances -> most similar window
        # normalize values
        max_distance = max(windows_distances, key=lambda p: p[1])[1]
        most_similar_windows = [(value[0], value[1] / max_distance) for value in windows_distances]

        # remove windows with score 0 (itself) and 1 (useless, too different)
        sorted_similar_windows = list(filter(lambda x: x[1] != 0.0 and x[1] != 1, most_similar_windows))

        # sort by distance to query_array
        # TODO: please use a sorted data structure rather than sorting at every estimation
        sorted_similar_windows = sorted(sorted_similar_windows, key=lambda tup: tup[1])

        # return the N most similar windows
        if len(sorted_similar_windows) > num_data_windows:
            return sorted_similar_windows[:num_data_windows]
        # HERE I RETURN THE WINDOWS WHICH DISTANCE FROM THE REFERENCE ONE (query_array) IS MINIMIZED
        return sorted_similar_windows

    @classmethod
    def calculate_estimation_weight(cls, windows_list: list[Tuple[DataWindow, float]]) -> (float, float):
        """
        Calculate the weight to be used for estimating a data value
        Ref: Eqs 2-3 https://doi.org/10.1109/ACCESS.2020.3028967
        :param windows_list: a list of tuple (DataWindow, float(score of dist)).
        :return: Numerator and Denominator of the weight
        """
        num = 0
        den = 0
        for entry in windows_list:
            # take the distance of window dw to the query_array (see estimate_data)
            # the distance is the similarity between the reference context window and a similar one
            # (reference -> the one for which  density must be estimated)
            dist = entry[1]
            num = num + ((1 - dist) * (entry[0].data.iat[-1] - entry[0].data.iat[-2]))
            den = den + (1 - dist)
        return num, den

    @classmethod
    def calculate_agent_confidence(cls, sensing_ag,
                                   query_buffer,
                                   num_data_windows_for_estimation,
                                   datawindow_size, strategy_id):
        pass

    @classmethod
    def estimate_data(cls, time, sensing_ag, query_buffer, num_data_windows_for_estimation,
                      datawindow_size, strategy_id, ground_truth_mask) -> float:
        pass
