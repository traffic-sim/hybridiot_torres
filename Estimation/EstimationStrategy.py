from dataclasses import dataclass

from Estimation.Estimator import Estimator


@dataclass(init=True, repr=True)
class EstimationStrategy:
    id: int
    estimator: Estimator
    name: str
    num_windows_for_estimation: int
    data_window_size: int
    qty_data_bootstrap: float  # in [0,1]
