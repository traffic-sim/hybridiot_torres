from dataclasses import dataclass

from Agent.State import State
from Estimation.EstimationStrategy import EstimationStrategy


@dataclass(init=True, repr=True)
class EstimationResult:
    estimate: float
    strategy: EstimationStrategy
    score: float
    new_state: State
